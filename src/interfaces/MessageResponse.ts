export default interface MessageResponse {
  data?: {};
  message?: string;
}
