export const extention = [
    { name: 'text/html',
      value: ['html', 'htm', 'shtml']
    },
    { name: 'text/css',
      value: ['css']
    },
    { name: 'text/xml',
    value: ['xml']
    },
    { name: 'image/gif',
    value: ['gif']
    },
    { name: 'image/jpeg',
    value: ['jpeg', 'jpg'] 
    },
    { name: 'application/x-javascript',
    value: ['js']
    },
    { name: 'application/atom+xml',
    value: ['atom']
    },
    { name: 'application/rss+xml',
    value: ['rss']
    },
    { name: 'text/mathml',
    value: ['mml']
    },
    { name: 'text/plain',
    value: ['txt']
    },
    { name: 'text/vnd.sun.j2me.app-descriptor',
    value: ['jad']
    },
    { name: 'text/vnd.wap.wml',
    value: ['wml']
    },
    { name: 'text/x-component',
    value: ['htc']
    },
    { name: 'image/png',
    value: ['png']
    },
    { name: 'image/tiff',
    value: ['tif', 'tiff']
    },
    { name: 'image/vnd.wap.wbmp',
    value: ['wbmp']
    },
    { name: 'image/x-icon',
    value: ['ico']
    },
    { name: 'image/x-jng',
    value: ['jng']
    },
    { name: 'image/x-ms-bmp',
    value: ['bmp']
    },
    { name: 'image/svg+xml',
    value: ['svg']
    },
    { name: 'image/webp',
    value: ['webp']
    },
    { name: 'application/java-archive',
    value: ['jar', 'war', 'ear']
    },
    { name: 'application/mac-binhex40',
    value: ['hqx']
    },
    { name: 'application/msword',
    value: ['doc']
    },
    { name: 'application/postscript',
    value: ['ps', 'eps', 'ai']
    },
    { name: 'application/rtf',
    value: ['rtf']
    },
    { name: 'application/vnd.ms-excel',
    value: ['xls']
    },
    { name: 'application/vnd.ms-powerpoint',
    value: ['ppt']
    },
    { name: 'application/vnd.wap.wmlc',
    value: ['wmlc']
    },
    { name: 'application/vnd.google-earth.kml+xml',
    value: ['kml']
    },
    { name: 'application/vnd.google-earth.kmz',
    value: ['kmz']
    },
    { name: 'application/x-7z-compressed',
    value: ['7z']
    },
    { name: 'application/x-cocoa',
    value: ['cco']
    },
    { name: 'application/x-java-archive-diff',
    value: ['jardiff']
    },
    { name: 'application/x-java-jnlp-file',
    value: ['jnlp']
    },
    { name: 'application/x-makeself',
    value: ['run']
    },
    { name: 'application/x-perl',
    value: ['pl', 'pm']
    },
    { name: 'application/x-pilot',
    value: ['prc', 'pdb']
    },
    { name: 'application/x-rar-compressed',
    value: ['rar']
    },
    { name: 'application/x-redhat-package-manager',
    value: ['rpm']
    },
    { name: 'application/x-sea',
    value: ['sea']
    },
    { name: 'application/x-shockwave-flash',
    value: ['swf']
    },
    { name: 'application/x-stuffit',
    value: ['sit']
    },
    { name: 'application/x-tcl',
    value: ['tcl', 'tk']
    },
    { name: 'application/x-x509-ca-cert',
    value: ['der', 'pem', 'crt']
    },
    { name: 'application/x-xpinstall',
    value: ['xpi']
    },
    { name: 'application/xhtml+xml',
    value: ['xhtml']
    },
    { name: 'application/zip',
    value: ['zip']
    },
    { name: 'application/octet-stream',
    value: ['bin', 'exe', 'dll', 'deb', 'dmg', 'eot', 'iso', 'img', 'msi', 'msp', 'msm']
    },
    { name: 'audio/midi',
    value: ['mid', 'midi', 'kar']
    },
    { name: 'audio/mpeg',
    value: ['mp3']
    },
    { name: 'audio/ogg',
    value: ['ogg']
    },
    { name: 'audio/x-realaudio',
    value: ['ra']
    },
    { name: 'video/3gpp',
    value: ['3gpp', '3gp']
    },
    { name: 'video/mpeg',
    value: ['mpeg', 'mpg']
    },
    { name: 'video/quicktime',
    value: ['mov']
    },
    { name: 'video/x-flv',
    value: ['flv']
    },
    { name: 'video/x-mng',
    value: ['mng']
    },
    { name: 'video/x-ms-asf',
    value: ['asx', 'asf']
    },
    { name: 'video/x-ms-wmv',
    value: ['wmv']
    },
    { name: 'video/x-msvideo',
    value: ['avi']
    },
    { name: 'video/mp4',
    value: ['m4v', 'mp4']
    }
  ];