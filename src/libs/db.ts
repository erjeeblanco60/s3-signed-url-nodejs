
import AWS from './aws'

const client = new AWS.DynamoDB.DocumentClient();
const TABLE = process.env.TABLE_NAME;

export default {
  get: (params) => {
    return client.get({...params, TableName: TABLE }).promise();
  },
  query: (params) => {
    return client.query({...params, TableName: TABLE }).promise()
  },
  put: (params) => {
    return client.put({...params, TableName: TABLE }).promise()
  },
  update: (params) => {
    return client.update({
      ...params,
      TableName: TABLE,
      ReturnValues: 'ALL_NEW' // returns updated record
    }).promise()
  },
  delete: (params) => {
    return client.delete({
      ...params,
      TableName: TABLE,
      ReturnValues: 'ALL_OLD' // returns deleted record
    }).promise()
  },
  toJSONUpdate: (data) => {
    delete data.PK;
    delete data.SK;
    let UpdateExpression = '';
    const ExpressionAttributeValues = {};
    const ExpressionAttributeNames = {};

    for (const key in data) {
      if (data.hasOwnProperty(key)) {
        if (UpdateExpression) {
          UpdateExpression += ', ';
        } else {
          UpdateExpression += 'SET';
        }

        UpdateExpression += `#${key}=:${key}`;
        ExpressionAttributeValues[`:${key}`] = data[key];
        ExpressionAttributeNames[`#${key}`] = key;
      }
    }

    return {
      UpdateExpression: UpdateExpression,
      ExpressionAttributeValues: ExpressionAttributeValues,
      ExpressionAttributeNames: ExpressionAttributeNames,
    };
  },
  batchGet: async ({TableName, Keys}) => {
    try {
      let index = 0;
      const arrayLength = Keys.length;
      const chunk_size = 100;
      let results = [];

      for (index = 0; index < arrayLength; index += chunk_size) {
        const myChunk = Keys.slice(index, index + chunk_size);

        const toGet = {
          RequestItems: {
            [TableName]: {
              Keys: myChunk
            }
          }
        };

        const batchGetResult = await client.batchGet(toGet).promise();
        results = results.concat(batchGetResult.Responses[TableName]);
      }

      return results;
    } catch (e) {
      throw (e);
    }
  },
};