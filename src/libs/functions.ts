import AWS from './aws'

import { PresignedPost } from 'aws-sdk/clients/s3';

import { S3 } from 'aws-sdk';


import { extention } from './extention';

const BUCKET_NAME = process.env.BUCKET_NAME;
const options: S3.ClientConfiguration = { signatureVersion: 'v4' };
const s3 = new S3(options);

export const getS3PresignedPost = async (filename) => {
  try {

    const s3Params: PresignedPost.Params = {
      Bucket: BUCKET_NAME,
      Expires: 300,
      Fields: {
        key: filename,
        acl: 'private',
      },
    };

    const ext = filename.split('.').pop();

    const matchedExtension = extention.find(data => data.value.includes(ext));

    if (matchedExtension) {
      s3Params.Fields['Content-Type'] = matchedExtension.name;
    }

    s3Params.Fields['Cache-Control'] = 'max-age=31536000';

    const signedPost = await functions.s3.createPresignedPost(s3Params);

    return {
      url: signedPost.url,
      fields: signedPost.fields,
      key: filename,
    };

  } catch (e) {
    console.log(e)
  }
};

export const getS3GetObjectSignedUrl = async (filename) => {

  try {

    const s3Params = {
      Bucket: BUCKET_NAME,
      Key: filename,
      Expires: 300,
    };

    return await functions.s3.getSignedUrl('getObject', s3Params);

  } catch (e) {
    return {
      success: 0,
      message: e.message,
    };
  }
};

export const deleteS3Object = async (filename, folder = '') => {
  try {
    if (folder && folder[folder.length - 1] !== '/') {
      folder += '/';
    }

    const s3Params = {
      Bucket: BUCKET_NAME,
      Key: folder + filename,
    };

    await functions.s3.deleteObject(s3Params).promise();

    return {
      success: 1,
    };
  } catch (e) {
    return {
      success: 0,
      message: e.message,
    };
  }
};


const functions = {
  s3,
  getS3PresignedPost,
  getS3GetObjectSignedUrl,
  deleteS3Object
};

export default functions;