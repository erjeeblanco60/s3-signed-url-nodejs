import express , { Request, Response }  from 'express';
import MessageResponse from '../interfaces/MessageResponse';
import db from '../libs/db';
import { v4 as uuid } from 'uuid';
import moment from 'moment';
import functions from '../libs/functions';

const router = express.Router();

router.get('/health', (req, res) => {
  const healthStatus = { status: 'ok' };
  res.json(healthStatus);
});

router.get('/files', async (req, res) => {

  try {
    let { Items: files } = await db.query({
      KeyConditionExpression: 'GSI1 = :gsi1 AND SK = :sk',
      ExpressionAttributeValues: {
        ':gsi1': 'files',
        ':sk': 'files',
      },
      IndexName: 'GSI1-SK-index',
    });

    res.json({
      message: 'List files',
      data: files,
    });

  } catch (error) {
  }
});

router.post('/upload-random', async (req, res) => {
  const data = req.body;

  try {
  
    const item = {
      PK: uuid(),
      name: data.name,
      type: data.type,
      SK: 'files',
      GSI1: 'files',
      extention: data.name.split('.').pop(),
      uploadedAt: moment(new Date()).format('YYYY-MM-DDTHH:mm:ss.SSSZ'),
      updatedAt: moment(new Date()).format('YYYY-MM-DDTHH:mm:ss.SSSZ'),
    };

    // save file to db
    await db.put({ Item: item });

    // get presigned post url
    const s3 = await functions.getS3PresignedPost(
      `${item.PK}.${item.name.split('.').pop()}`
    );
     
    return res.json({
      message: 'File uploaded successfully',
      data: item,
      s3
    });

  } catch (e) {
    return {
      message: e.message,
    }
  }

});

// get file signed URL
router.get('/file/:id', async (req, res) => {
  const { id } = req.params;
  try {
    return res.json(functions.getS3GetObjectSignedUrl(
      id
    ));
  } catch (e) {
    return {e,
    req
    }
  }

});

router.delete('/file/:id', async (req, res) => {
  const { id } = req.params;
  
  try {
    const Key = {
      PK: id.substring(0, id.lastIndexOf('.')),
      SK: 'files',
    };

    const { Item: file } = await db.get({
      Key,
    });

    if (!file) {
      return res.json({
        message: 'File not found',
      });
    }

    await db.delete({
      Key,
    });

    // delete s3 file
    await functions.deleteS3Object(id);

    return res.json({
      message: 'File deleted successfully',
    });

  } catch (e) {
    return {
      message: e.message,
    }
  }
});

export default router;