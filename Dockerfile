# Use an official Node.js runtime as the base image
FROM node:16

# Increase the JavaScript heap memory limit
ENV NODE_OPTIONS="--max-old-space-size=4096"

# Set the working directory in the container
WORKDIR /app

# Copy package.json and package-lock.json to the container
COPY package*.json ./

# Install application dependencies
RUN apt-get update && apt-get install -y curl
RUN curl -sL https://deb.nodesource.com/setup_14.x | bash -
RUN apt-get install -y nodejs

# Install application dependencies
RUN npm install

# Copy the rest of the application code to the container
COPY . .

# Expose a port (optional, if your application needs to listen on a specific port)
EXPOSE 4545

# Specify the command to run your application
CMD ["npm", "run", "start"]
