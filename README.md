# Introduction

In this project, the main focus is on building an API that allows users to upload random files to a cloud storage service. The core functionality is implemented using the `/api/upload-random` route. Additionally, other routes are provided to handle file retrieval, deletion, and listing of all files.

### Uploading Files with Presigned URLs

The API utilizes presigned URLs to facilitate file uploads. When a client sends a `POST` request to the `/api/upload-random` route, a presigned URL is generated using the `functions.getS3PresignedPost()` method. This presigned URL acts as a temporary authentication token that grants the client permission to upload a file directly to the cloud storage service, such as AWS S3. By using presigned URLs, the API can delegate the file upload process directly to the client, reducing server-side load and improving performance.

### Retrieving Files and Providing Download URLs

To retrieve a specific file, the API exposes the `/api/file/:id` route. When a client sends a `GET` request to this route, the file's information is fetched from the database using `db.get()`. Alongside the file information, a signed URL for downloading the file is generated using `functions.getS3GetObjectSignedUrl()`. This signed URL ensures that only authorized users can access the file and provides a secure mechanism for downloading the file directly from the cloud storage service.

### File Deletion and Listing

The API also offers routes for deleting files and listing all uploaded files. The `/api/file/:id` route handles `DELETE` requests to remove a specific file from both the database and the cloud storage service. On the other hand, the `/api/files` route responds to `GET` requests by retrieving all files from the database using `db.query()`.

By providing these additional routes, users can manage their uploaded files effectively, including deleting unwanted files and obtaining a list of all the files they have uploaded.

Overall, this API implementation enables seamless uploading, retrieval, and management of random files using presigned URLs for secure and efficient file operations.

To ensure secure file uploads and retrieval, I have implemented a private bucket with Cross-origin resource sharing (CORS) policy. The CORS policy defines the rules for accessing the bucket from different origins. In this case, the policy allows requests from a specific origin, which is `http://localhost:4200`.

The CORS policy configuration is as follows:

```json
[
    {
        "AllowedHeaders": [
            "*"
        ],
        "AllowedMethods": [
            "POST",
            "GET"
        ],
        "AllowedOrigins": [
            "http://localhost:4200"
        ],
        "ExposeHeaders": []
    }
]
```

The policy ensures that only requests originating from `http://localhost:4200` are allowed to perform `POST` and `GET` operations on the bucket. This restriction adds an extra layer of security by limiting access to authorized origins.

By implementing this CORS policy, unauthorized sources are blocked from interacting with the bucket. This helps prevent potential security risks and unauthorized file uploads or retrievals.

Please refer to the image below for a visual representation of the setup:

![S3 CORS Policy](./images/S3.png)

Please note that you can customize the CORS policy according to your specific requirements, such as allowing access from different origins or specifying additional headers.

# Directory Structure

### Directories

#### `/api`
The `api` directory contains the route handlers for the API endpoints. It organizes the routes into separate files or modules to keep the codebase modular and maintainable.

#### `/interfaces`
The `interfaces` directory contains TypeScript interfaces or type definitions. It defines the structure and types used throughout the codebase, providing a consistent way of defining data shapes and contracts.

#### `/libs`
The `libs` directory contains libraries or utility functions used in the application. In this case, it includes libraries for interacting with AWS S3 and DynamoDB services. These libraries provide reusable functionality and encapsulate the logic related to these services.

### Files

#### `app.ts`
The `app.ts` file is responsible for configuring the Express application. It sets up the middleware, defines the routes, and applies any necessary configurations. It may include additional setup related to Express, such as body parsing, logging, error handling, or authentication.

#### `index.ts`
The `index.ts` file is the entry point of the application. It listens for incoming requests on a specific port (in this case, port 4545) and starts the Express server. It typically imports the `app` module from `app.ts` and invokes the server listening functionality.

#### `middlewares.ts`
The `middlewares.ts` file contains middleware functions that are used in the application. Middleware functions intercept incoming requests or outgoing responses and can perform various operations such as modifying the request/response objects, handling errors, or adding additional functionality. In this case, it contains a middleware function for error handling.

These directories and files help organize the codebase and separate concerns, making it easier to navigate and maintain the application.

---


## Routes and Their Purposes

### `/health` (GET)

This route is responsible for checking the health status of the server. When accessed, it returns a JSON payload containing `{ "status": "ok" }`. It can be used to verify that the server is up and running.

### `/files` (GET)

This route retrieves a list of files from the database. It queries the database using the `db.query()` method, specifying the conditions to retrieve the files. The retrieved files are then returned as a JSON response.

### `/upload-random` (POST)

This route allows the uploading of a file to the server. It expects the file details (name and type) in the request body. Upon receiving the request, it generates a unique identifier (`PK`) using `uuid()` and creates an item object with the file details. The item is then stored in the database using the `db.put()` method. Additionally, it retrieves a presigned URL for uploading the file to an S3 bucket using the `functions.getS3PresignedPost()` method. The response includes a JSON payload confirming the successful upload, along with the file details and the presigned URL.

### `/file/:id` (GET)

This route retrieves a specific file from the database based on the provided `id` parameter. It first extracts the primary key (`PK`) from the `id` by removing the file extension. It then queries the database using the `db.get()` method to fetch the file information. If the file is found, it retrieves a signed URL for downloading the file from the S3 bucket using the `functions.getS3GetObjectSignedUrl()` method. The file information, including the signed URL, is returned as a JSON response.

### `/file/:id` (DELETE)

This route deletes a specific file from the server and the associated entry in the database. It expects the `id` parameter to identify the file. It retrieves the file information from the database using the `db.get()` method. If the file exists, it is deleted from the database using the `db.delete()` method, and the corresponding file is removed from the S3 bucket using the `functions.deleteS3Object()` method. A JSON response is returned to confirm the successful deletion of the file.

These routes handle different functionalities such as retrieving files, uploading files, fetching file details, and deleting files. The code utilizes a combination of database operations and interactions with an S3 bucket for file storage and retrieval.

## Includes API Server utilities:

* [morgan](https://www.npmjs.com/package/morgan)
  * HTTP request logger middleware for node.js
* [helmet](https://www.npmjs.com/package/helmet)
  * Helmet helps you secure your Express apps by setting various HTTP headers. It's not a silver bullet, but it can help!
* [cors](https://www.npmjs.com/package/cors)
  * CORS is a node.js package for providing a Connect/Express middleware that can be used to enable CORS with various options.

Development utilities:

* [typescript](https://www.npmjs.com/package/typescript)
  * TypeScript is a language for application-scale JavaScript.
* [ts-node](https://www.npmjs.com/package/ts-node)
  * TypeScript execution and REPL for node.js, with source map and native ESM support.
* [nodemon](https://www.npmjs.com/package/nodemon)
  * nodemon is a tool that helps develop node.js based applications by automatically restarting the node application when file changes in the directory are detected.
* [eslint](https://www.npmjs.com/package/eslint)
  * ESLint is a tool for identifying and reporting on patterns found in ECMAScript/JavaScript code.
* [typescript-eslint](https://typescript-eslint.io/)
  * Tooling which enables ESLint to support TypeScript.

## Setup

```
npm install
```

## Lint

```
npm run lint
```

## Development

```
npm run dev
```
